provider "aws" {
  region     = "us-east-1"
}
variable "subnet-cidr-block" {
  description = "subnet cidr block"
}

variable "vpc-cidr-block" {
  description = "vpc cidr block"
}

resource "aws_vpc" "development-vpc" {
  cidr_block = var.vpc-cidr-block
  tags = {
    Name: "development"
    vpc_env: "dev"
  }
}

resource "aws_subnet" "dev-subnet-1" {
  cidr_block = var.subnet-cidr-block
  vpc_id = aws_vpc.development-vpc.id
  availability_zone = "us-east-1a"
  tags = {
    Name: "subnet-1-dev"
  }
}

output "dev-vpc-id" {
  value = aws_vpc.development-vpc.id
}

output "dev-subnet-id" {
  value = aws_subnet.dev-subnet-1.id
}

